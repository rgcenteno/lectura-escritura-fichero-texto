/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lecturaescrituraficheros.io;

import java.io.File;
import java.io.IOException;


/**
 *
 * @author Rafael González Centeno
 */
public class FileManager {
    
    public static String loadTextFile(File f) throws IOException{
        StringBuilder txt = new StringBuilder();
        //Tenemos el fichero seleccionado, muestra el contenido en jTextArea
        try(java.io.BufferedReader in = new java.io.BufferedReader(new java.io.FileReader(f))){            
            String line = in.readLine();
            while(line != null){
                txt.append(line).append("\n");
                line = in.readLine();
            }        
        }
        return txt.toString();
    }
    
    public static boolean saveFile(File f, String txt) throws IOException{
        boolean correcto = false;
        try(java.io.BufferedWriter out = new java.io.BufferedWriter(new java.io.FileWriter(f))){            
            //Tenemos el fichero seleccionado, muestra el contenido en jTextArea
            out.write(txt);            
            correcto = true;            
        }        
        return correcto;
    }
}
